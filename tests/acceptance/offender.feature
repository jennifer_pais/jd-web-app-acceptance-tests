Feature: I should be able to add new Offender

Scenario: Insert new Offender

Given I am on page "/"
And I fill in "//input[@id='login_username']" with "jenrppmuser@zincdigital.com"
And I fill in "//input[@id='login_password']" with "pa55w0rd"
And I click Login Button "//input[@id='button_login_main']"
Then I should see "Dashboard"
And I click on Security section "//li[@id='ftSecurity']/a/span"
And I click OffenderAddNew "//button[@id='Offenders_addNew']/span"