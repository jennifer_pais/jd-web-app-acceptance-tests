Feature: As a valid user I should be able to add Office LIT

Scenario: Add new Office LIT

Given I am on page "/"
Then I should see "Please Login"
And I fill in "//input[@id='login_username']" with "jenrppmuser@zincdigital.com"
And I fill in "//input[@id='login_password']" with "pa55w0rd"
And I click Login Button "//input[@id='button_login_main']"
Then I should see "Dashboard"
And I click on OfficeDescLITDashboard "//div[@id='mainDashboardSection']/div[2]/div/div/div[2]/div/div/div/ul/li[5]/div/div/a/div/span"
And I click AddOfficeDescsLIT "//div[@id='221']/div/div/div/div[2]/div[2]/div/div/button/span"
And I click JointIncidentNO "//div[@id='no_right']/a"
Then I click Next Button "//div[@id='page_grp_st10']/div/div/button/div"
Then I click Next Button "//div[@id='page_grp_st11']/div/div/button/div/span"
And I click Location DropDown "//div[@id='ddS3']/div/ol/button/span[2]"
And I fill in "(//input[@type='text'])[7]" with "zinc"
And I click "//a[contains(text(),'Zinc HQ ( Z001) (AU Region 1,-())')]"
Then I click Next Button "//div[@id='page_grp_st1']/div/div/button/div/span"
And I click Outcome "//div[@id='LIT_1064_12']/div/div/button/span/span/span/img"
Then I click Next Button "//div[@id='page_grp_st2']/div/div/button/div/span"
And I click Incident Details(Questions) "//div[@id='Inbound_0_1828']/div/div/button"
And I click Location "#UK__1827 .organge-btn"
