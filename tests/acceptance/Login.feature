Feature: As a valid user I should be able to login into JD Sports

Scenario: Signing in with an authorised user
 Given I am on page "/"
 Then I should see "Please Login"
 And I fill in "//input[@id='username']" with "jenrppmuser@zincdigital.com"
 And I fill in "password" with "pa55w0rd"
 And I click Login Button "//input[@id='button_login']"
 Then I should see "Dashboard"