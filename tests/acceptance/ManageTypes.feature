Feature: JD Admin - Store Incidents Module: Unable to 'Manage Types'

Scenario: As a user I should be able to view manage types form/ window and should be able to edit types

Given I am on page "/"
Then I should see "Please enter your details below."
And I fill in "kt_login_user" with "jennifer.pais@zincdigital.com"
And I fill in "kt_login_password" with "jennifer2019"
And I click JD Admin Login Button "kt_login"
Then I should see "Welcome"
And I click on Operations "//a[contains(text(),'Operations')]"
And I click on store Incidents "//a[contains(text(),'Store Incidents')]"
Then I should see "Store Incidents"
And I click cog filter "//img[@id='filter_area_button']"
And I cilck on Manage Types "//a[contains(@href, '/index_module.php?mod=reports_incident_type')]"
#Then I should see "Incident Reports - Types"