Feature: JD Admin - Extract Incident Reports

Scenario: As a user I should be able to extract incident reports

Given I am on page "/"
Then I should see "Please enter your details below."
And I fill in "kt_login_user" with "jennifer.pais@zincdigital.com"
And I fill in "kt_login_password" with "jennifer2019"
And I click JD Admin Login Button "kt_login"
Then I should see "Welcome"
And I click on Reports&Stats "(//a[contains(text(),'Reports & Stats')])[2]"
And I click on Incident Reports tab "//div[@id='box1']/div[2]/div/ul/li/span/span"
And I click on Exract Report "//a[contains(text(),'Extract Report')]"
Then I should see "entries"