Feature: I should be able to add new vehicle

Scenario: Insert new vehicle

Given I am on page "/"
And I fill in "//input[@id='login_username']" with "jenrppmuser@zincdigital.com"
And I fill in "//input[@id='login_password']" with "pa55w0rd"
And I click Login Button "//input[@id='button_login_main']"
Then I should see "Dashboard"
And I click on Security section "//li[@id='ftSecurity']/a/span"
And I click Add New Vehicle "//button[@id='Vehicles_addNew']/span/span"
Then I should see "Add New Vehicle:"
And I fill in "//input[@id='vehicles_value']" with "Test1"
And I click on Full Plate Radio "//label[contains(.,'Full')]"
And I click PossibleFullPlate YES "//div[@id='Vehicle_Details']/div[2]/div/div/div/div[2]/div/label"
And I fill in "//textarea[@id='add_vehicle_description']" with "Test Description"
And I fill in "//input[@id='add_vehicle_make']" with "MAKE"
And I fill in "//input[@id='add_vehicle_model']" with "MODEL"
And I fill in "//input[@id='add_vehicle_colour']" with "COLOR"
And I click Venue dropdown "//ol[@id='venue']/button/span"
And I fill in "(//input[@type='text'])[6]" with "zinc"
And I click a Venue "//a[contains(.,'Zinc HQ')]"
And I click SaveVehicle "//button[@id='add_vehicle_save_btn']/div/span"
Then I should see "Vehicle added successfully."
