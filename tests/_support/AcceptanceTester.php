<?php

use Codeception\Lib\Generator\Actions;
use Facebook\WebDriver\Interactions\Touch\WebDriverMoveAction;
use Facebook\WebDriver\Interactions\WebDriverActions;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

   /**
    * Define custom actions here
    */
    /**
     * @Given I am on :arg1
     */
    public function iAmOn($arg1)
    {
        return $this->amOnUrl($arg1);
        $this->wait(5);
    }
    /**
     * @Given I am on page :arg1
     */
    public function iAmOnPage($URL)
    {
        return $this->amOnPage($URL);
        $this->wait(3);
    }
     /**
    * @Then I should see :arg1
    */
    public function iShouldSee($arg1)
    {
        $this->wait(5);
        return $this->see($arg1);
        $this->wait(3);
    }

   /**
    * @Then I fill in :arg1 with :arg2
    */
    public function iFillInWith($arg1, $arg2)
    {
        $this->wait(8);
        $this->fillField($arg1, $arg2);
    }
    /**
     * @Then I click Login Button :arg1
     */
    public function iClickLoginButton($arg1)
    {
        $this->click($arg1);
        $this->wait(120);
    }


     /**
     * @Then I click on Operations :arg1
     */
    public function iClickOnOperations($arg1)
    {
        $this->click($arg1);
    }

   /**
    * @Then I click on store Incidents :arg1
    */
    public function iClickOnStoreIncidents($arg1)
    {
        $this->click($arg1);
    }
 /**
     * @Then I click JD Admin Login Button :arg1
     */
    public function iClickJDAdminLoginButton($arg1)
    {
        $this->click($arg1);
    }
  /**
     * @Then I click cog filter :arg1
     */
    public function iClickCogFilter($arg1)
    {
        $this->click($arg1);
    }

   /**
    * @Then I cilck on Manage Types :arg1
    */
    public function iCilckOnManageTypes($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);
    }

    #--------------------------Extract Reports-------------------------------------------------
 /**
     * @Then I click on Reports&Stats :arg1
     */
    public function iClickOnReportsStats($arg1)
    {
        $this->click($arg1);
    }
   /**
    * @Then I click on Incident Reports tab :arg1
    */
    public function iClickOnIncidentReportsTab($arg1)
    {
        $this->scrollTo($arg1);
        $this->click($arg1);
    }

   /**
    * @Then I click on Exract Report :arg1
    */
    public function iClickOnExractReport($arg1)
    {
        $this->scrollTo($arg1);
        $this->wait(3);
        $this->click($arg1);
        $this->wait(10);
    }
#--------------------------------------Add Store Incidents----------------------------------------
/**
     * @Given I click on Add New Store Incidents :arg1
     */
    public function iClickOnAddNewStoreIncidents($arg1)
    {
        $this->wait(10);
        $this->click($arg1);
        $this->wait(3);
    }

   /**
    * @Then I click Next Button :arg1
    */
    public function iClickNextButton($arg1)
    {
        $this->scrollTo($arg1);
        $this->click($arg1);
    }

   /**
    * @Then I click Location DropDown :arg1
    */
    public function iClickLocationDropDown($arg1)
    {
        $this->wait(5);
        $this->scrollTo($arg1);
        $this->click($arg1);
    }

   /**
    * @Then I click :arg1
    */
    public function iClick($arg1)
    {
        $this->click($arg1);
    }
 /**
     * @Then I click OutcomeActual :arg1
     */
    public function iClickOutcomeActual($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);   
    }

   /**
    * @Then I click Type Theft :arg1
    */
    public function iClickTypeTheft($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }

   /**
    * @Then I click TheftCarriedOutBy :arg1
    */
    public function iClickTheftCarriedOutBy($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }

   /**
    * @Then I click WorkPlaceVoilenceORThreateningActs :arg1
    */
    public function iClickWorkPlaceVoilenceORThreateningActs($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }

   /**
    * @Then I click IncidentTookPlace :arg1
    */
    public function iClickIncidentTookPlace($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }

   /**
    * @Then I click NatureOfVoilence :arg1
    */
    public function iClickNatureOfVoilence($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }

   /**
    * @Then I click BulkTheft :arg1
    */
    public function iClickBulkTheft($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }

   /**
    * @Then I click TheftLocation :arg1
    */
    public function iClickTheftLocation($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }

   /**
    * @Then I click TheftMethod :arg1
    */
    public function iClickTheftMethod($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }

   /**
    * @Then I click EASSystemActive :arg1
    */
    public function iClickEASSystemActive($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }

   /**
    * @Then I click MetalDetectionSystemActive :arg1
    */
    public function iClickMetalDetectionSystemActive($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }

#-----------------------------Add New Vehicle----------------------------------------
/**
     * @Then I click on Security section :arg1
     */
    public function iClickOnSecuritySection($arg1)
    {
        $this->click($arg1);
    }

 /**
     * @Then I click Add New Vehicle :arg1
     */
    public function iClickAddNewVehicle($arg1)
    {
        $this->wait(5);
        $this->scrollTo($arg1);
        $this->click($arg1);
    }
 /**
     * @Then I click on Full Plate Radio :arg1
     */
    public function iClickOnFullPlateRadio($arg1)
    {
        $this->wait(5);
        $this->click($arg1);
    }

   /**
    * @Then I click PossibleFullPlate YES :arg1
    */
    public function iClickPossibleFullPlateYES($arg1)
    {
        $this->wait(5);
        $this->click($arg1);
    }

   /**
    * @Then I click Venue dropdown :arg1
    */
    public function iClickVenueDropdown($arg1)
    {
        $this->wait(5);
        $this->click($arg1);
    }

   /**
    * @Then I click a Venue :arg1
    */
    public function iClickAVenue($arg1)
    {
        $this->wait(5);
        $this->click($arg1);
    }
     /**
     * @Then I click SaveVehicle :arg1
     */
    public function iClickSaveVehicle($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }
#------------------------------------Victims & Witness-------------------------------------------
/**
     * @Then I click VictimWitnessAddNew :arg1
     */
    public function iClickVictimWitnessAddNew($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }
/**
     * @Then I click VictimWitnessTitle :arg1
     */
    public function iClickVictimWitnessTitle($arg1)
    {
        $this->wait(3);
        $this->click($arg1);    
    }

   /**
    * @Then I click Title :arg1
    */
    public function iClickTitle($arg1)
    {
        $this->wait(3);
        $this->click($arg1);  
    }

   /**
    * @Then I click CountryDropdown :arg1
    */
    public function iClickCountryDropdown($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);  
    }

   /**
    * @Then I click VictimWitnessVenueDropdown :arg1
    */
    public function iClickVictimWitnessVenueDropdown($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);
    }
 /**
     * @Then I click VictimWitnessSave :arg1
     */
    public function iClickVictimWitnessSave($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);
    }

   /**
    * @Then I click WitnessOption :arg1
    */
    public function iClickWitnessOption($arg1)
    {
        $this->wait(3);
        $this->click($arg1); 
    }
#---------------------------------Office Descs + LIT-----------------------------------
/**
     * @Then I click AddOfficeDescsLIT :arg1
     */
    public function iClickAddOfficeDescsLIT($arg1)
    {
        $this->wait(10);
        $this->click($arg1);
    }

   /**
    * @Then I click JointIncidentNO :arg1
    */
    public function iClickJointIncidentNO($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);
    }
 /**
     * @Then I click on OfficeDescLITDashboard :arg1
     */
    public function iClickOnOfficeDescLITDashboard($arg1)
    {
        $this->wait(3);
        $this->click($arg1);
    }
/**
     * @Then I click Outcome :arg1
     */
    public function iClickOutcome($arg1)
    {
        $this->wait(3);
        $this->click($arg1);
    }
 /**
     * @Then I click Incident Details(Questions) :arg1
     */
    public function iClickIncidentDetailsQuestions($arg1)
    {
        $this->wait(3);
        $this->click($arg1);
    }

   /**
    * @Then I click Location :arg1
    */
    public function iClickLocation($arg1)
    {
        $this->wait(3);
        $this->clickWithLeftButton(['css' => '#UK__1827 .organge-btn']);
        $this->click($arg1);
    }

#----------------------------------Offender----------------------------------------------
/**
     * @Then I click OffenderAddNew :arg1
     */
    public function iClickOffenderAddNew($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1);
    }
     /**
     * @Then I click on DashboardViewOffender :arg1
     */
    public function iClickOnDashboardViewOffender($arg1)
    {
        $this->wait(5);
        $this->click($arg1);
    }

 /**
     * @Then I click GenderDropdown :arg1
     */
    public function iClickGenderDropdown($arg1)
    {
        $this->wait(3);
        $this->click($arg1); 
    }

   /**
    * @Then I select Gender :arg1
    */
    public function iSelectGender($arg1)
    {
        $this->wait(3);
        $this->click($arg1);     
    }

   /**
    * @Then I click EthnicityDropdown :arg1
    */
    public function iClickEthnicityDropdown($arg1)
    {
        $this->wait(3);
        $this->click($arg1); 
    }

   /**
    * @Then I select Ethnicity :arg1
    */
    public function iSelectEthnicity($arg1)
    {
        $this->wait(3);
        $this->click($arg1); 
    }

   /**
    * @Then I click HeightDropdown :arg1
    */
    public function iClickHeightDropdown($arg1)
    {
        $this->wait(3);
        $this->click($arg1); 
    }

   /**
    * @Then I select Height :arg1
    */
    public function iSelectHeight($arg1)
    {
        $this->wait(3);
        $this->click($arg1); 
    }

   /**
    * @Then I click Glasses :arg1
    */
    public function iClickGlasses($arg1)
    {
        $this->wait(3);
        $this->click($arg1); 
    }

   /**
    * @Then I click Piercings :arg1
    */
    public function iClickPiercings($arg1)
    {
        $this->wait(3);
        $this->click($arg1); 
    }

   /**
    * @Then I click Tattoos :arg1
    */
    public function iClickTattoos($arg1)
    {
        $this->wait(3);
        $this->click($arg1); 
    }

   /**
    * @Then I click BuildDropdown :arg1
    */
    public function iClickBuildDropdown($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1,  '0', '1');
        $this->wait(3);
        $this->click($arg1); 
    }

   /**
    * @Then I select Build :arg1
    */
    public function iSelectBuild($arg1)
    {
        $this->wait(3);
        $this->click($arg1); 
    }

   /**
    * @Then I click HairColorDropdown :arg1
    */
    public function iClickHairColorDropdown($arg1)
    {
        $this->wait(3);
        $this->scrollTo($arg1);
        $this->click($arg1); 
    }

   /**
    * @Then I select HairColor :arg1
    */
    public function iSelectHairColor($arg1)
    {
        $this->wait(3);
        $this->click($arg1); 
    }
/**
     * @Then I click on FacialHairDropdown :arg1
     */
    public function iClickOnFacialHairDropdown($arg1)
    {
        $this->wait(3);
        $this->click($arg1); 
    }

   /**
    * @Then I click FacialHair :arg1
    */
    public function iClickFacialHair($arg1)
    {
        $this->wait(3);
        $this->click($arg1);
    }


}
